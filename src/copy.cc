// After building the rust project with
// ```
// cargo build --release
// ```
// you can compile this with
// ```
// mkoctfile src/copy.cc -L./target/release -lrust_oct
// ```
// from the parent directory of the `src` directory.

#include <octave/oct.h>
#include "copy.h"

// A wrapper function around the function in rust.
RowVector copy_internal(RowVector input) {
    // Create the data structure to be passed to the function written in Rust.
    DoubleSlice input_slice = {
        .head = input.fortran_vec(), // `fortran_vec()` extracts the "raw pointer"
        .len = input.numel()         // the number of elements in the row vector.
    };
    
    // Call the function written in Rust.
    DoubleVec computed_result = rust_function(input_slice);
    
    // Create a rowvector to hold the result.
    // As far as I understand, octave offers no way to construct an array
    // from a raw pointer.
    RowVector output = RowVector(computed_result.len);
    
    // Copy the bytes from the input to the output.
    memcpy(output.fortran_vec(), computed_result.head, computed_result.len * sizeof(double));
    
    // Cleanup the memory created on the Rust side.
    drop_double_vec(computed_result);
    
    return output;
}

// Call a C++ macro.
// Here "copy" is the name of the function as we can use in octave.
// It must match the filename of the ".oct" file, so that octave knows where to look
// for the function.    
DEFUN_DLD (copy, args, , "copy(input), returns a ropy of a row vector")
{
	if (args.length() != 1)
		print_usage ();
	else
	{
		RowVector input = args(0).row_vector_value();
		if (! error_state)
		{
			return octave_value(copy_internal(input));
		}
	}
	return octave_value_list();
}
