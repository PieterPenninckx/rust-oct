// This is a direct translation of the `#[repr(C)]` and `extern "C"` items
// that are defined in Rust.
extern "C" {
    // Note that the order of the fields in the struct must match the order used on the Rust side.
    struct DoubleSlice {
        double* head;
        size_t len;
    };
    
    struct DoubleVec {
        double* head;
        size_t len;
        size_t cap;
    };
    
    void drop_double_vec(DoubleVec double_vec);
    
    DoubleVec rust_function(DoubleSlice double_slice);
}
