extern crate libc;

use std::slice;
use std::mem;

#[repr(C)]
pub struct DoubleSlice {
    head: *const libc::c_double,
    len: libc::size_t,
}

impl DoubleSlice {
    pub unsafe fn into_slice(self) -> &'static [f64] {
        #[allow(unused_unsafe)]
        unsafe {
            slice::from_raw_parts(self.head, self.len)
        }
    }
}

#[repr(C)]
pub struct DoubleVec {
    head: *mut libc::c_double,
    len: libc::size_t,
    cap: libc::size_t,
}

impl DoubleVec {
    pub fn from_vec(mut vec: Vec<f64>) -> Self {
        let result = DoubleVec {
            head: vec.as_mut_ptr(),
            len: vec.len(),
            cap: vec.capacity(),
        };
        mem::forget(vec);
        result
    }
}

#[no_mangle]
pub unsafe extern "C" fn drop_double_vec(double_vec: DoubleVec) {
    #[allow(unused_unsafe)]
    unsafe {
        mem::drop(Vec::from_raw_parts(double_vec.head, double_vec.len, double_vec.cap));
    }
}

#[no_mangle]
pub extern "C" fn rust_function(double_slice: DoubleSlice) -> DoubleVec
{
    let mut vec = Vec::new();
    vec.extend_from_slice(unsafe {
        double_slice.into_slice()
    });
    DoubleVec::from_vec(vec)
}
