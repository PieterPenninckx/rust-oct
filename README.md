Rust-oct
========

An example demonstrating how [Rust](https://www.rust-lang.org/) code can be called from
[GNU Octave](https://www.gnu.org/software/octave/).

Prerequisites
-------------

First you need to be able to generate `.oct` files at all.

In order to be able to generate `.oct` files, you can install

* `liboctave-dev` for if you use Debian GNU/Linux,
* `octave` if you use Arch Linux,
* the package that includes the `mkoctfile` command if you use another system.

Compiling
---------

After building the rust project with

```bash
cargo build --release
```

you can compile this with

```bash
mkoctfile src/copy.cc -L./target/release -lrust_oct
```

from the parent directory of the `src` directory.
This generates the `.oct` file.

Using
-----

The previous steps generate a file named `copy.oct`.
If you start GNU Octave from the folder that contains this `copy.oct` file, or if the
`copy.oct` file is in the 
[load path of GNU Octave](https://octave.org/doc/v4.2.2/Manipulating-the-Load-Path.html), 
you can use it as follows:

```octave
a = [1,2,3];
b = copy(a);
```

Now `b` is `[1,2,3]` as well (yes, it's a very simple example).

Notes
-----
In the command line 
```bash
mkoctfile src/copy.cc -L./target/release -lrust_oct
```

`rust_oct` is the name of the crate, so if you adapt the name of the crate, make sure to adapt
the command line accordingly.

(Un)license
-----------

Licensed under the "Unlicense". See the `LICENSE` file for details.
